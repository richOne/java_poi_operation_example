package com.zhao.common.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 基于JDK 实现一个简单的动态代理
 */
public class DynamicProxy {
    public static void main(String[] args) {
        /**
         * 定义一个InvocationHandler实例，它负责实现接口的方法调用；
         * 通过Proxy.newProxyInstance()创建interface实例，它需要3个参数：
         * 使用的ClassLoader，通常就是接口类的ClassLoader；
         * 需要实现的接口数组，至少需要传入一个接口进去；
         * 用来处理接口方法调用的InvocationHandler实例。
         * 将返回的Object强制转型为接口。
         */
        InvocationHandler handler=new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("代理类名称："+proxy.getClass().getName());
                if(method.getName().equals("food")){
                    System.out.println("吃了 "+args[0]);
                    return null;
                }
                if(method.getName().equals("number")){
                    System.out.println("数字 ： "+args[0]+"  "+args[1]);
                   return (int)args[0]+(int)args[1];

                }
                return null;
            }
        };

        Eat eat= (Eat) Proxy.newProxyInstance(Eat.class.getClassLoader(),new Class[]{Eat.class},handler);
        eat.food("空气");
        System.out.println("总和 "+eat.number(3, 16));

    }
}
