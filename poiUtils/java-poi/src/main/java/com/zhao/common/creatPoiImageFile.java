package com.zhao.common;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 插入图片
 */
public class creatPoiImageFile {
    public static void main(String[] args) throws IOException {
        //1.创建workbook工作簿
        Workbook wb = new XSSFWorkbook();
        //2.创建表单Sheet
        Sheet sheet = wb.createSheet("test");
        //读取图片流
        FileInputStream stream = new FileInputStream("D:\\projectCode\\poiUtils\\timg.jpg");
        byte[] bytes = IOUtils.toByteArray(stream); //poi 提供的流转字节数组
        //读取图片到二进制数组
        stream.read(bytes);
        //向Excel添加一张图片,并返回该图片在Excel中的图片集合中的下标
        int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG); //参数1 ：图片字节数据 ，图片类型
        //绘图工具类
        CreationHelper helper = wb.getCreationHelper();

        //创建一个绘图对象
        Drawing<?> patriarch = sheet.createDrawingPatriarch();
        //创建锚点,设置图片坐标
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(0);//从0开始
        anchor.setRow1(0);//从0开始
        //创建图片
        Picture picture = patriarch.createPicture(anchor, pictureIdx); //参数 excel 中的图片位置 ，传递到内存中的图片索引
        picture.resize(); //自适应渲染图片

        //3.文件流
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("D:\\projectCode\\poiUtils\\testImage.xlsx");
            //4.写入文件
            wb.write(fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
