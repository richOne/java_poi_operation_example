package com.zhao.common;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.StringJoiner;

public class test {
    private  String name;
    public   String age;
    public test(){
      super();
    }
    public test(String name,String age){
        this.name=name;
        this.age=age;
    }

    public String getName() {
        return name;
    }
    //https://www.liaoxuefeng.com/wiki/1252599548343744/1271993169413952
    public static void main(String[] args) throws Exception {




        String[] names = {"Bob", "Alice", "Grace"};
        StringJoiner sj = new StringJoiner(", ", "Hello ", "!");
        for (String name : names) {
            sj.add(name);
        }
        System.out.println(sj.toString());
        System.out.println(new test().getClass());
        Class<test> testClass = test.class;
        System.out.println(testClass);
        System.out.println(Class.forName("com.zhao.common.test"));
        System.out.println(testClass == Class.forName("com.zhao.common.test"));

        System.out.println("Class name: " + testClass.getName());
        System.out.println("Simple name: " + testClass.getSimpleName());
        if (testClass.getPackage() != null) {
            System.out.println("Package name: " + testClass.getPackage().getName());
        }
        System.out.println("is interface: " + testClass.isInterface());
        System.out.println("is enum: " + testClass.isEnum());
        System.out.println("is array: " + testClass.isArray());
        System.out.println("is primitive: " + testClass.isPrimitive());
        // 创建一个test实例:
        test s = (test) testClass.newInstance();
        System.out.println(s);
        System.out.println("----------");
        System.out.println(testClass.getField("age").getName());
        test t=new test("zhao","25");
        System.out.println(testClass.getField("age").get(t));
        for (Field field : testClass.getDeclaredFields()) {
            System.out.println(field);
        }
        System.out.println("----------");
        System.out.println(testClass.getMethod("getName"));
        System.out.println(testClass.getMethod("getName").invoke(t));
        System.out.println("----------");
       // 获取构造方法
        Constructor cons1 = testClass.getConstructor(String.class,String.class);
        // 调用构造方法:
        test n1 = (test) cons1.newInstance("zhao","24");
        System.out.println(n1.getName());

        System.out.println(testClass.getSuperclass());

    }
}
