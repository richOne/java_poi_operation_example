package com.zhao.common.util;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 用于 Excel下载的工具类
 */
public class DownloadUtils {
    /**
     *
     * @param byteArrayOutputStream 输出字节流
     * @param response
     * @param returnName 输出到客户端的文件名
     * @throws IOException
     */
    public   void download(ByteArrayOutputStream byteArrayOutputStream, HttpServletResponse response, String returnName) throws IOException {
        response.setContentType("application/octet-stream");
        returnName = response.encodeURL(new String(returnName.getBytes(),"iso8859-1"));			//保存的文件名,必须和页面编码一致,否则乱码
//        response.addHeader("Content-Disposition","attachment;filename=total.xls");
        response.addHeader("Content-Disposition","attachment;filename="+returnName);
        response.setContentLength(byteArrayOutputStream.size());
        response.addHeader("Content-Length", "" + byteArrayOutputStream.size());
        ServletOutputStream outputStream = response.getOutputStream();	//取得输出流
        byteArrayOutputStream.writeTo(outputStream);					//写到输出流
        byteArrayOutputStream.close();									//关闭
        outputStream.flush();											//刷数据
    }
}
