package com.zhao.common;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 加载excel 中的内容
 * POI中getLastRowNum() 和getLastCellNum()的区别
 *  hssfSheet.getLastRowNum();//最后一行行标，比行数小1
 * hssfSheet.getRow(k).getLastCellNum();//获取列数，比最后一列列标大1
 */
public class loadExcel {
    //单元格样式
    public static void main(String[] args) throws Exception {
        //1.创建workbook工作簿
        Workbook wb = new XSSFWorkbook("D:\\projectCode\\poiUtils\\demo.xlsx");
        //2.获取sheet 从0开始
        Sheet sheet = wb.getSheetAt(0);
        Row row = null;
        Cell cell = null;
        //循环所有行
        for (int rowNum = 0; rowNum <= sheet.getLastRowNum(); rowNum++) {
            row = sheet.getRow(rowNum);
            StringBuilder sb = new StringBuilder();
            //循环每行中的所有单元格
            for (int cellNum = 0; cellNum < row.getLastCellNum(); cellNum++) {
                cell = row.getCell(cellNum);
                if(cell==null){
                    continue;
                }
                sb.append(getValue(cell)).append("-");
            }
            System.out.println(sb.toString());
        }
    }

    //获取数据
    private static Object getValue(Cell cell) {
        Object value = null;
        //获取excel 中值的类型
        switch (cell.getCellType()) {
            case STRING: //字符串类型
                value = cell.getStringCellValue();
                break;
            case BOOLEAN: //boolean类型
                value = cell.getBooleanCellValue();
                break;
            case NUMERIC: //数字类型（包含日期和普通数字）
                //DateUtil.isCellDateFormatted 判断数据类型是否为日期
                if (DateUtil.isCellDateFormatted(cell)) {
                    value = cell.getDateCellValue();
                } else {
                    value = cell.getNumericCellValue();
                }
                break;
            case FORMULA: //公式类型
                value = cell.getCellFormula();
                break;
            default:
                break;
        }
        return value;
    }
}
