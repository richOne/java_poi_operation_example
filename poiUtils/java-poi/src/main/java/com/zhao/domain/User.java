package com.zhao.domain;

import com.zhao.common.util.ExcelAttribute;
import com.zhao.common.util.ExcelImportAnnotation;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DecimalFormat;

@Data
@NoArgsConstructor
public class User {
    @ExcelAttribute(sort = 0)
    @ExcelImportAnnotation(sort = 2)
    private  String name;
    @ExcelAttribute(sort = 1)
    @ExcelImportAnnotation(sort = 3)
    private  Integer password;

    public User(Object[] args){
        /** DecimalFormat 用法
         * https://www.jianshu.com/p/b3699d73142e
         * Integer.valueOf 返回的时包装类  Integer.parseInt() 返回的是int
         */
        //因为传进来的args 的赋值是从1开始的
        this.name=args[1].toString();   //new DecimalFormat("#").format(args[2]).toString();
        //因为从Excel中读取的数字是double类型的 所以不能用 Integer.valueOf
        this.password=new Double(args[2].toString()).intValue();
    }

}
