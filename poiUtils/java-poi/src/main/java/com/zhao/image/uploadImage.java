package com.zhao.image;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 图片上传
 */
@RestController
public class uploadImage {
    @PostMapping("uploadImage")
    public String uploadImg(@RequestParam("file")MultipartFile file) throws IOException {
        //使用DataURL 的形式存储图片（对图片byte数组进行base64编码）
       String encode = Base64.encode(file.getBytes());
       //得到 DataURL 的形式的图片  直接在浏览器的网址部分输入即可展示或者 前端 的img的 src属性
        return "data:image/png;base64,"+encode;
    }
}
