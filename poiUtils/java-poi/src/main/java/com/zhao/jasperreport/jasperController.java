package com.zhao.jasperreport;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * web端导出 通过 jaspersoft-studio工具已经编译好的jasper的文件模板 生成 pdf
 */
@RestController
public class jasperController {
    @GetMapping("exportPDF")
    public  void exportPDF(HttpServletRequest request ,HttpServletResponse response) throws Exception {
        //1.引入 jasper 文件
        Resource resource=new ClassPathResource("template-pdf/zhaoPDF.jasper");
        FileInputStream fis=new FileInputStream(resource.getFile());
        //2.创建jasper 文件 向获取到的模板jasper文件填充数据
        //new JREmptyDataSource() 不写的话 输出的pdf永远都为空
        /**
         * fis 文件输入流
         * new HashMap<>() 向模板中输入的 参数
         * new JREmptyDataSource() 数据源 和数据库中的数据源不同  填充模板的数据来源（connection jAVAbean map）
         *      没有数据源 填充空的数据源  JREmptyDataSource
         */
        JasperPrint print = JasperFillManager.fillReport(fis, new HashMap<>(), new JREmptyDataSource());

        //3.将jasper 以pdf的形式输出
        /**
         * 参数 0 jasperPrint
         *      1 输出流
         */
        ServletOutputStream outputStream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(print,outputStream);
        outputStream.flush();
    }
    @GetMapping("exportPDFH")
    public  void exportPDFH(HttpServletRequest request ,HttpServletResponse response) throws Exception {
        //参考 ：https://blog.csdn.net/weixin_42528266/article/details/103769077
        //1.引入 jasper 文件
        Resource resource=new ClassPathResource("template-pdf/zhaoPDF1.jasper");
        FileInputStream fis=new FileInputStream(resource.getFile());
        //2.创建jasper 文件 向获取到的模板jasper文件填充数据
        //new JREmptyDataSource() 不写的话 输出的pdf永远都为空
        /**
         * fis 文件输入流
         * new HashMap<>() 向模板中输入的 参数
         * new JREmptyDataSource() 数据源 和数据库中的数据源不同  填充模板的数据来源（connection jAVAbean map）
         *      没有数据源 填充空的数据源  JREmptyDataSource
         */
        Map parame = new HashMap<>();
        //设置参数 参数的key = 模板中使用的parameters参数的name
        parame.put("name","Map填充数据");
        parame.put("password","10086");
        JasperPrint print = JasperFillManager.fillReport(fis, parame, new JREmptyDataSource());

        //3.将jasper 以pdf的形式输出
        /**
         * 参数 0 jasperPrint
         *      1 输出流
         */
        ServletOutputStream outputStream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(print,outputStream);
        outputStream.flush();
    }
}

