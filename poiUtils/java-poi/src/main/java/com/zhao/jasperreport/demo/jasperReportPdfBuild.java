package com.zhao.jasperreport.demo;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.view.JasperViewer;

import java.util.HashMap;

/**
 *  利用 jasperReport 工具生成 pdf
 */
public class jasperReportPdfBuild {
    public static void main(String[] args) {
        createJasper();
      // createJrprint();
       // showPdf();
 
    }

    //1.将pdf模板编译为Jasper文件
    public static void createJasper(){
        try{
            String path = "D:\\projectCode\\poiUtils\\java-poi\\src\\main\\java\\com\\zhao\\jasperreport\\demo\\Design.jrxml";
            JasperCompileManager.compileReportToFile(path);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //2.将Jasper文件 和数据进行填充 或Jrprint
    public static void createJrprint(){
        try{
            String path = "D:\\projectCode\\poiUtils\\java-poi\\src\\main\\java\\com\\zhao\\jasperreport\\demo\\JsPDF.jasper";
            //通过空参数和空数据源进行填充
            JasperFillManager.fillReportToFile(path,new HashMap(),new JREmptyDataSource());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //3.预览
    public static void showPdf(){
        try{
            String path = "D:\\projectCode\\poiUtils\\java-poi\\src\\main\\java\\com\\zhao\\jasperreport\\demo\\JsPDF.jrprint";
            JasperViewer.viewReport(path,false);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
