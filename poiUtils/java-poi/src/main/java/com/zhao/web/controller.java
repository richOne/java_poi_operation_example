package com.zhao.web;

import com.zhao.common.util.DownloadUtils;
import com.zhao.common.util.ExcelExportUtil;
import com.zhao.common.util.ExcelImportUtil;
import com.zhao.common.util.SheetHandler;
import com.zhao.domain.User;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
public class controller {
    /**
     * 基于sax事件模型读取百万Excel数据
     */
    @PostMapping("readMillionExcelData")
    public void  readMillionExcelData(@RequestParam(name = "file") MultipartFile file) throws Exception {

        //1.根据excel报表获取基于事件模型的 OPCPackage对象 其实就是把这个Excel文件以压缩包的形式打开成xml文件 PackageAccess.READ 只读
        OPCPackage opcPackage = OPCPackage.open(file.getInputStream());
        //2.创建XSSFReader
        XSSFReader reader = new XSSFReader(opcPackage);
        //3.获取SharedStringTable对象
        SharedStringsTable table = reader.getSharedStringsTable();
        //4.获取styleTable对象
        StylesTable stylesTable = reader.getStylesTable();
        //5.创建Sax的xmlReader对象
        XMLReader xmlReader = XMLReaderFactory.createXMLReader();
        //6.注册自定义的事件处理器
        XSSFSheetXMLHandler xmlHandler = new XSSFSheetXMLHandler(stylesTable, table, new SheetHandler(), false);
        xmlReader.setContentHandler(xmlHandler);
        //7.逐行读取 得到的是所有的 sheet
        XSSFReader.SheetIterator sheetIterator = (XSSFReader.SheetIterator) reader.getSheetsData();
        while (sheetIterator.hasNext()) {
            InputStream stream = sheetIterator.next(); //每一个sheet的流数据
            InputSource is = new InputSource(stream);  //每一个sheet对象
            xmlReader.parse(is);
        }

    }

    /**
     * Excel 报表导出
     * SXSSFWorkbook 导出 支持百万导出的 工作簿 类
     * 不支持模板打印
     */
    @GetMapping("sXSSFExport")
    public void  sXSSFExport(HttpServletResponse response) throws IOException {
        //1.创建workbook工作簿   1000 阈值 也就是每1000 行就服务器的硬盘上存一次，默认 100
        SXSSFWorkbook wb = new SXSSFWorkbook(1000);
        //2.创建表单Sheet
        Sheet sheet = wb.createSheet("百万导出");
        String[] title="序号，名字，密码".split("，"); //怎么创建数组都行
        //3.创建行对象，从0开始 ,创建标题
        Row row = sheet.createRow(0);
        //4.创建单元格，从0开始
        Cell cell = null;
        //标题
        /**   标题的第二种实现
         *    for (int i = 0; i < title.length; i++) {
         *             //4.创建单元格，从0开始
         *             row.createCell(i);
         *             //5.单元格写入数据
         *             cell.setCellValue(title[i]);
         *         }
         */
        int indexTitle=0;
        for (String s : title) {
            //4.创建单元格，从0开始
            cell = row.createCell(indexTitle++);
            //5.单元格写入数据
            cell.setCellValue(s);
        }

        //人为构造的数据，实际是要从数据库中查的
        List<User> users=new ArrayList<>();
        User user= new User();
        for (int i = 0; i <1 ; i++) {
            user.setName("洪真英");
            user.setPassword(123456);
            users.add(user);
        }

        int indexContext=1;
        int index=1;
        //内容
        for (int j=0;j<1048575;j++) {
            for (User user1 : users) {
                //创建每一行，同excel的第二行开始
                row = sheet.createRow(indexContext++);
                //第一列
                cell = row.createCell(0);
                //写入数据 序号
                cell.setCellValue(index++);
                //第2列
                cell = row.createCell(1);
                cell.setCellValue(user1.getName());
                //第2列
                cell = row.createCell(2);
                cell.setCellValue(user1.getPassword());
            }
        }
        ByteArrayOutputStream os=new ByteArrayOutputStream();
        wb.write(os);
        new DownloadUtils().download(os,response,"百万数据导出.xlsx");

    }


    /**
     * 使用抽取的Excel的工具类进行导入 基于模板
     */
    @PostMapping("ExcelUtilImport")
    public void  ExcelUtilImport(@RequestParam(name = "file") MultipartFile file) throws Exception {
        List<User> list = new ExcelImportUtil(User.class).readExcel(file.getInputStream(), 1, 2);
        System.out.println(list);
    }




    /**
     * 使用抽取的Excel的工具类进行导出 基于模板
     */
    @GetMapping("ExcelUtilExport")
    public void  ExcelUtilExport(HttpServletResponse response) throws Exception {
        //1.获取数据
        //人为构造的数据，实际是要从数据库中查的
        List<User> users=new ArrayList<>();
        User user= new User();
        for (int i = 0; i <3 ; i++) {
            user.setName("洪真英");
            user.setPassword(123456);
            users.add(user);
        }

        //2.加载模板
        Resource resource = new ClassPathResource("template-Excel/exceltUtilTemplate.xlsx");
        FileInputStream fis = new FileInputStream(resource.getFile());

        //3.通过 工具类下载文件
        //因为内容样式和要写的内容都在下标2也就时excel中的第三行
        new ExcelExportUtil(User.class,2,2)
                /**
                 * 参数 1：HttpServletResponse
                 *      2：文件输入流
                 *      3：封装好的对象
                 *      4：文件名
                 */
        .export(response,fis,users,"反射的形式导出.xlsx");

    }



    /**
     * 采用 提前制作好的excel模板导出 数据
     */
    @GetMapping("exportTemplate")
    public void  exportTemplate(HttpServletResponse response) throws IOException {
        //1.获取数据
        //人为构造的数据，实际是要从数据库中查的
        List<User> users=new ArrayList<>();
        User user= new User();
        for (int i = 0; i <3 ; i++) {
            user.setName("洪真英");
            user.setPassword(123456);
            users.add(user);
        }

        //2.加载模板
        Resource resource = new ClassPathResource("template-Excel/exceltTemplate.xlsx");
        FileInputStream fis = new FileInputStream(resource.getFile());

        //3.根据模板创建工作簿
        Workbook wb = new XSSFWorkbook(fis);
        //4.读取工作表
        Sheet sheet = wb.getSheetAt(0);
        //5.抽取公共样式 , 因为前2行 为标题 第三行是数据 下标为2
        Row row = sheet.getRow(2);
        CellStyle styles[] = new CellStyle[row.getLastCellNum()];
        Cell cell = null;
        for (int i = 0; i < row.getLastCellNum(); i++) {
            cell = row.getCell(i);
            styles[i] = cell.getCellStyle();
        }
        //6.构造单元格
        int rowIndex=2;
        int indexNumber=1;
        for (User user1:users) {
            //创建每一行，同excel的第二行开始
            row=sheet.createRow(rowIndex++);
            //第一列
            cell = row.createCell(0);
            //设置单元格样式
            cell.setCellStyle(styles[0]);
            //写入数据 序号
            cell.setCellValue(indexNumber++);
            //第2列
            cell = row.createCell(1);
            cell.setCellStyle(styles[1]);
            cell.setCellValue(user1.getName());
            //第3列
            cell = row.createCell(2);
            cell.setCellStyle(styles[2]);
            cell.setCellValue(user1.getPassword());
            //第4列 自己乱加的一列时间 user对象中没有
            cell = row.createCell(3);
            cell.setCellStyle(styles[3]);
            cell.setCellValue(LocalDate.now().toString());
        }
        //7.下载
        ByteArrayOutputStream os=new ByteArrayOutputStream();
        wb.write(os);
        new DownloadUtils().download(os,response,"webExcel模板导出.xlsx");
    }
    /**
     * Excel 报表导出
     */
    @GetMapping("export")
    public void  export(HttpServletResponse response) throws IOException {
        //1.创建workbook工作簿
        Workbook wb = new XSSFWorkbook();
        //2.创建表单Sheet
        Sheet sheet = wb.createSheet("web");
        String[] title="序号，名字，密码".split("，"); //怎么创建数组都行
        //3.创建行对象，从0开始 ,创建标题
        Row row = sheet.createRow(0);
        //4.创建单元格，从0开始
        Cell cell = null;
       //标题
        /**   标题的第二种实现
         *    for (int i = 0; i < title.length; i++) {
         *             //4.创建单元格，从0开始
         *             row.createCell(i);
         *             //5.单元格写入数据
         *             cell.setCellValue(title[i]);
         *         }
         */
        int indexTitle=0;
        for (String s : title) {
            //4.创建单元格，从0开始
            cell = row.createCell(indexTitle++);
            //5.单元格写入数据
            cell.setCellValue(s);
        }

        //人为构造的数据，实际是要从数据库中查的
        List<User> users=new ArrayList<>();
         User user= new User();
        for (int i = 0; i <3 ; i++) {
            user.setName("洪真英");
            user.setPassword(123456);
            users.add(user);
        }

        int indexContext=1;
        int index=1;
        //内容
        for (User user1:users) {
            //创建每一行，同excel的第二行开始
            row=sheet.createRow(indexContext++);
            //第一列
            cell = row.createCell(0);
            //写入数据 序号
            cell.setCellValue(index++);
            //第2列
            cell = row.createCell(1);
            cell.setCellValue(user1.getName());
            //第2列
            cell = row.createCell(2);
            cell.setCellValue(user1.getPassword());
        }
        ByteArrayOutputStream os=new ByteArrayOutputStream();
        wb.write(os);
        new DownloadUtils().download(os,response,"webExcel导出.xlsx");

    }



    /**
     * excel 文件上传
     * postman 上传文件 操作指南https://jingyan.baidu.com/article/425e69e614f472be14fc166f.html
     */
    @PostMapping("upload")
    public String upload(@RequestParam(name = "file") MultipartFile file) throws IOException {
        //1.解析Excel
        //1.1.根据Excel文件创建工作簿
        Workbook wb = new XSSFWorkbook(file.getInputStream());
        //1.2.获取Sheet
        Sheet sheet = wb.getSheetAt(0);//参数：索引
        //1.3.获取Sheet中的每一行，和每一个单元格
        //2.获取用户数据列表
        List<User> list = new ArrayList<>();
        System.out.println("最后一行的下标 :" + sheet.getLastRowNum());
        for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
            Row row = sheet.getRow(rowNum);//根据索引获取每一个行
//            System.out.println("列数"+row.getLastCellNum());
            Object[] values = new Object[row.getLastCellNum()];
            for (int cellNum = 1; cellNum < row.getLastCellNum(); cellNum++) {
                Cell cell = row.getCell(cellNum);
                Object value = getCellValue(cell);
                values[cellNum] = value;
            }
            User user = new User(values);
            list.add(user);
        }
        //3.批量保存用户
//        userService.saveAll(list,companyId,companyName);

        return "SUCCESS";
    }

    public static Object getCellValue(Cell cell) {
        //1.获取到单元格的属性类型
        CellType cellType = cell.getCellType();
        //2.根据单元格数据类型获取数据
        Object value = null;
        switch (cellType) {
            case STRING:
                value = cell.getStringCellValue();
                break;
            case BOOLEAN:
                value = cell.getBooleanCellValue();
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    //日期格式
                    value = cell.getDateCellValue();
                } else {
                    //数字
                    value = cell.getNumericCellValue();
                }
                break;
            case FORMULA: //公式
                value = cell.getCellFormula();
                break;
            default:
                break;
        }
        return value;
    }
}
